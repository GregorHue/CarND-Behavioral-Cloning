#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 29 18:19:45 2017

@author: gregor
"""

import csv
import cv2
import numpy as np
import matplotlib.image as mpimg
from keras.layers import Dense, Flatten, Lambda, Activation, Dropout
from keras.layers.convolutional import Convolution2D
from keras.models import Sequential
from keras.optimizers import Adam

lines = []
# import of the driving_log
with open('./data/driving_log.csv') as csvfile:
    reader = csv.reader(csvfile)
    for line in reader:
        lines.append(line)
        
images = []
measurements = []
for i in range(1,len(lines)):
    line = lines[i]
    source_path = line[0]
    filename = source_path.split('/')[-1]
    current_path = './data/IMG/' + filename
    # import of the center image
    image = mpimg.imread(current_path)
    images .append(image)
    measurement = float(line[3])
    measurements.append(measurement)
    # correction of the steering angle for right and left image
    correction = 0.10
    source_path = line[1]
    filename = source_path.split('/')[-1]
    current_path = './data/IMG/' + filename
    # import of the image of the left camera
    image = mpimg.imread(current_path)  
    images .append(image)
    measurements.append(measurement + correction)
    source_path = line[2]
    filename = source_path.split('/')[-1]
    current_path = './data/IMG/' + filename
    # import of the image of thr right camera
    image = mpimg.imread(current_path)    
    images .append(image)
    measurements.append(measurement - correction)

augmented_images, augmented_measurements = [], []
for image, measurement in zip(images, measurements):
    augmented_images.append(image)
    augmented_measurements.append(measurement)
    # horizontal flip of the image
    augmented_images.append(cv2.flip(image,1))
    augmented_measurements.append(-measurement)


# converting to numpy arrays
X_train = np.array(augmented_images)
y_train = np.array(augmented_measurements)

# creation of the neural net
model = Sequential()

model.add(Lambda(lambda x: x / 127.5 - 1.0, input_shape=(160, 320, 3)))

model.add(Convolution2D(24, 5, 5, border_mode='same', subsample=(2, 2)))
model.add(Activation('relu'))

model.add(Convolution2D(36, 5, 5, border_mode='same', subsample=(2, 2)))
model.add(Activation('relu'))

model.add(Convolution2D(48, 5, 5, border_mode='same', subsample=(2, 2)))
model.add(Activation('relu'))

model.add(Convolution2D(64, 3, 3, border_mode='same', subsample=(1, 1)))
model.add(Activation('relu'))

model.add(Convolution2D(64, 3, 3, border_mode='same', subsample=(1, 1)))
model.add(Activation('relu'))

model.add(Flatten())

model.add(Dense(1164))
model.add(Dropout(0.9))
model.add(Activation('relu'))

model.add(Dense(100))
model.add(Activation('relu'))

model.add(Dense(50))
model.add(Activation('relu'))

model.add(Dense(10))
model.add(Activation('relu'))

model.add(Dense(1))

model.summary()

# compiling
model.compile(optimizer=Adam(1e-4), loss="mse", )

# predicting
model.fit(X_train, y_train, validation_split = 0.2, shuffle = True, nb_epoch = 1)

model.save('model.h5')



    