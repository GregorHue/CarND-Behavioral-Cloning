# **Behavioral Cloning Project**

## Goals

The goals / steps of this project are the following:
* Use the simulator to collect data of good driving behavior
* Build, a convolution neural network in Keras that predicts steering angles from images
* Train and validate the model with a training and validation set
* Test that the model successfully drives around track one without leaving the road
* Summarize the results with a written report

## Rubric Points
Here I will consider the [rubric points](https://review.udacity.com/#!/rubrics/432/view) individually and describe how I addressed each point in my implementation.

---

### Files Submitted & Code Quality

#### 1. Submission includes all required files and can be used to run the simulator in autonomous mode

My project includes the following files:
* model.py containing the script to create and train the model
* drive.py for driving the car in autonomous mode
* model.h5 containing a trained convolution neural network
* writeup_report.md or writeup_report.pdf summarizing the results

#### 2. Submission includes functional code
Using the Udacity provided simulator and my drive.py file, the car can be driven autonomously around the track by executing
```sh
python drive.py model.h5
```

#### 3. Submission code is usable and readable

The model.py file contains the code for training and saving the convolution neural network. The file shows the pipeline I used for training and validating the model, and it contains comments to explain how the code works.

### Model Architecture and Training Strategy

#### 1. An appropriate model architecture has been employed

My model consists of a convolution neural network with 5x5 and 3x3 filter sizes and depths between 24 and 64.

The model includes RELU layers to introduce nonlinearity, and the data is normalized in the model using a Keras lambda layer.

#### 2. Attempts to reduce overfitting in the model

Data augmentation was used in order to reduce overfitting in the model.

The model was tested by running it through the simulator and ensuring that the vehicle could stay on the track.

#### 3. Model parameter tuning

The model used an adam optimizer with a learning rate of 0.0001.

#### 4. Appropriate training data

The [data](https://d17h27t6h515a5.cloudfront.net/topher/2016/December/584f6edd_data/data.zip) provided by Udacity was chosen to keep the vehicle driving on the road. The downloaded file had to be put into the root folder of the project. I used a combination of the images captured by the center camera, the left and the right camera.

For details about how I created the training data, see the next section.

### Model Architecture and Training Strategy

#### 1. Solution Design Approach

I decided to try a model architecture published by nvidia [here](https://devblogs.nvidia.com/parallelforall/deep-learning-self-driving-cars).

After training the final step was to run the simulator to see how well the car was driving around track one.

The vehicle was able to drive autonomously around track one without leaving the road.

#### 2. Model Architecture in Detail

Here is a visualization of the model architecture published by nvidia [here](https://devblogs.nvidia.com/parallelforall/deep-learning-self-driving-cars).
Note that I used different image sizes. My input image size was 160 x 320. Accordingly the image sizes in the layers of the neural net were different.

![architecture](./images/architecture.png)

#### 3. Creation of the Training Set & Training Process

I only used the data provided by Udacity. One data set consists of the image of the center camera, the steering angle at this moment and two additional images of the left and right camera. Here is an example of an image of the center camera.

![center](./images/center.png)

Here is an image of the left camera

![left](./images/left.png)

and here an image of the right camera

![right](./images/right.png)

Then I had to figure out which amount I had to add to the steering angle (for the left image) or subtract from the steering angle (for the right image) in order to take the different point of view into account. After trying various correction amounts in the training process I ended up with an amount of 0.1 .

In the data there were much more data sets with an negative steering angle than with a positive angle. Therefore I flipped images and angles thinking that this would prevent the model from overfitting in the training process.

After the collection process, I had 48216 images of size 160x320 pixels. I then converted these images in one big numpy array. I converted the labels (actually the steering angles) in one numpy array too.

I finally randomly shuffled the data set and put 20% of the data into a validation set.

I used this training data for training the model with a Nvidia Geforce 1080 Titan X GPU. Since no memory exhaustion error arised there was no need of implementing a generator or using the keras build-in generator. The validation set helped to determine if the model was over- or underfitting. The ideal number of epochs was only one epoch because the validation accuracy decreased in the second epoch. I used an adam optimizer so that manually training the learning rate wasn't necessary.
